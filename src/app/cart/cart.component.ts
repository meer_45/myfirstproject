import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {


  items;
  checkoutForm:FormGroup;
  nameControl;
  myName;
  constructor(
    private cartService: CartService,
    private formBuilder: FormBuilder
  ) { 

    this.items = this.cartService.getItem();
    this.myForm();
    
  }

  myForm(){
    this.checkoutForm= this.formBuilder.group({
      name:this.formBuilder.control(''),
      address:'',
      mobile:''
    });

    this.nameControl= this.checkoutForm.get('name') as FormControl;
  }
  
  

  ngOnInit() {
    this.items= this.cartService.getItem();
    
  }


  onSubmit(){
    //console.log(this.checkoutForm.value);
    //process checkout data
   // console.warn('Your order has been submitted', customerData);
   // this.items = this.cartService.clearCart();
 //   this.checkoutForm.reset();
    console.log(this.myName);
  }

}
