import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductListComponent } from './product-list/product-list.component';
import { MemberListComponent } from './member-list/member-list.component';
import { ProductAlertComponent } from './product-alert/product-alert.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { CartComponent } from './cart/cart.component';
import { ShippingComponent } from './shipping/shipping.component';


const routes: Routes = [
  { path: 'products', component: ProductListComponent },
  { path: 'memberlist', component: MemberListComponent },
  { path: 'topbar', component:TopBarComponent},
  { path: 'products/:productId',component:ProductDetailsComponent},
  { path: '', component:ProductListComponent }, 
  { path: 'cart', component: CartComponent},
  { path: 'shipping', component:ShippingComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
